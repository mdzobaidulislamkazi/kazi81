import express from "express";
const app = express();
import cors from "cors";
import morgan from "morgan";
import swaggerUI from "swagger-ui-express";
import swaggerDocs from "./mergeYaml";

app.use(express.json());
app.use(express.urlencoded({ extended: true }));
app.use(express.static("public"));
app.use(express.json());
app.use(cors());
app.use(morgan("dev"));

app.use("/api-docs", swaggerUI.serve,  swaggerUI.setup(swaggerDocs));



const port = process.env.PORT || 4000;

app.get("/", (_req, res) => {
  res.send("<h1>Hello Kazi Byte!, Welcome to Kazi Byte API</h1>");
});



app.listen(port, () => {
  console.log(`Server is running on port ${port}`);
});

export default app;
