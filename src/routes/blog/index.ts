import express from "express";
const blog = express.Router();
import { createBlog, getAll } from "@/controllers/blog";

blog.post("/create", createBlog);

blog.get("/blogs", getAll);

export default blog;


console.log("Routes Loaded is blog folder")