import { Request, Response, NextFunction } from "express";
import Blog from "@/models/blog/Blog";
import blogSchema from "@/schemas/blog/blogSchema"; '@/schemas/blog/blogSchema'


const createBlog = (req: Request, res: Response, next: NextFunction) => {
    // body validator 
    const parsedBody = blogSchema.safeParse(req.body)

    if (!parsedBody.success) {
        // return error
        return
        res.status(400).json({ error: parsedBody.error })
    }

    // create blog

    const blog = new Blog({
        ...parsedBody.data
    })

    res.status(200).json({ success: true, data: blog })


};

export default createBlog;
