import { z } from "zod";
import { Types } from "mongoose";

// Zod schema for Category
const categorySchema = z.object({
  title: z.string().trim(),
  slug: z.string().trim(),
  createdAt: z.date().optional(),
  updatedAt: z.date().optional(),
});

// TypeScript types
type Category = z.infer<typeof categorySchema>;

export { categorySchema, Category };
