import { z } from "zod";
import { Types } from "mongoose";

// Zod schema
const blogSchema = z.object({
  authorId: z.instanceof(Types.ObjectId).optional(),
  title: z.string(),
  metaTitle: z.string().trim(),
  slug: z.string().trim(),
  description: z.string(),
  image: z.string(),
  author: z.string(),
  views: z.number().default(0),
  date: z.date().default(() => new Date()),
  createdAt: z.date().optional(),
  updatedAt: z.date().optional(),
});

export default blogSchema;
