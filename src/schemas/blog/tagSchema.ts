import { z } from "zod";

// Zod schema for Tag
const tagSchema = z.object({
  title: z.string().trim(),
  slug: z.string().trim(),
  description: z.string().trim().min(2).max(10),
  createdAt: z.date().optional(),
  updatedAt: z.date().optional(),
});

// TypeScript types
type Tag = z.infer<typeof tagSchema>;

export { tagSchema, Tag };
