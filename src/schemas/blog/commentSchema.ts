import { z } from "zod";
import { Types } from "mongoose";

// Zod schema for Comment
const commentSchema = z.object({
  blogId: z.instanceof(Types.ObjectId).optional(),
  authorId: z.instanceof(Types.ObjectId).optional(),
  name: z.string().trim(),
  comment: z.string().trim(),
  createdAt: z.date().optional(),
  updatedAt: z.date().optional(),
});

// TypeScript types
type Comment = z.infer<typeof commentSchema>;

export { commentSchema, Comment };
